﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObstacleCleared : MonoBehaviour {

	private ScoreCount getScoreText;

	void Start(){
		getScoreText = GameObject.Find ("Score").GetComponent<ScoreCount>();
	}

	void OnTriggerEnter2D(Collider2D col){
		GameObject obj = col.gameObject;

		if (obj.GetComponent<Plane> ()) {
			getScoreText.IncrementScore ();
		}
	}
}
