﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneSelection : MonoBehaviour {

	public Sprite[] Planes;

	private Image plane;

	int planeNumber = 0;

	void Start(){
		plane = GetComponent<Image> ();
		plane.sprite = Planes [PlayerPrefs.GetInt ("PlaneChoice")];
	}

	public void NextPlane(){
		planeNumber = PlayerPrefs.GetInt ("PlaneChoice");
		planeNumber++;
		if (planeNumber == Planes.Length) {
			planeNumber = 0;
		}
		plane.sprite = Planes[planeNumber];
		PlayerPrefs.SetInt ("PlaneChoice", planeNumber);
	}
}
